[![pipeline status](https://gitlab.mpcdf.mpg.de/votca/votca/badges/master/pipeline.svg)](https://gitlab.mpcdf.mpg.de/votca/votca/commits/master)
[![coverage report](https://gitlab.mpcdf.mpg.de/votca/votca/badges/master/coverage.svg)](https://gitlab.mpcdf.mpg.de/votca/votca/commits/master)

To install:

```
prefix=WHERE/TO/INSTALL/VOTCA
version=master # or 'stable' or 'v1.4.1'
git clone -b ${version} https://gitlab.mpcdf.mpg.de/votca/votca
cd votca
mkdir build
cd build
cmake -DBUILD_CSGAPPS=ON -DBUILD_CTP=ON -DBUILD_CSG_MANUAL=ON -DBUILD_CSG_TUTORIALS=ON -DCMAKE_INSTALL_PREFIX=${prefix} ..
make -j<number of cores>
make install
```
