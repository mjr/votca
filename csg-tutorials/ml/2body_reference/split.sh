#!/bin/bash

#splits the 10000 frame trajcetory into 10 chunks of 1000 frames each
#each frame consists of 100 atoms and 109 lines in the dump file (1000*109=109000)

mkdir trajectory_split
cd trajectory_split

for i in {1..10..1}
do	
  head -n "$(($i*109000))" ../lj.dump | tail -n 109000 > lj_$i.dump
done
