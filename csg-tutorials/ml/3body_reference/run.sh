#! /bin/bash -e

#run the LAMMPS simulation (needs a current LAMMPS version compiled with the user pair_style sw/table)
lmp < spce.in > spce.out

#rerun the LAMMPS simulation with only 3-body interactions switched on (needs a current LAMMPS version compiled with the user pair_style sw/table)
lmp < spce_rerun_3body.in > spce_rerun_3body.out

#run script to split the trajectory into 10 parts
./split.sh
