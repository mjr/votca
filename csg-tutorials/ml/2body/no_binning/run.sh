#! /bin/bash -e

#training of the ML model
csg_ml --no-map --options settings_train.xml --top ../train.dump --trj ../train.dump --nframes 100 > train.out

#testing of the ML model
csg_ml --no-map --options settings_test.xml --top ../test.dump --trj ../test.dump --nframes 10 > test.out
