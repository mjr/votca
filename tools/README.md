[![Codacy Badge](https://api.codacy.com/project/badge/Grade/afbb4ad86ff846799b7b87dc1f1ab4c6)](https://app.codacy.com/app/JoshuaSBrown/tools?utm_source=github.com&utm_medium=referral&utm_content=votca/tools&utm_campaign=Badge_Grade_Dashboard)
[![codecov](https://codecov.io/gh/votca/tools/branch/master/graph/badge.svg)](https://codecov.io/gh/votca/tools)
[![Build Status](https://travis-ci.org/votca/tools.svg?branch=master)](https://travis-ci.org/votca/tools)
[![pipeline status](https://gitlab.com/votca/tools/badges/master/pipeline.svg)](https://gitlab.com/votca/tools/commits/master)

This is the tools module of the VOTCA package.

Further information on VOTCA can be found at <http://www.votca.org>

You can contact the VOTCA Development Team at devs@votca.org.
