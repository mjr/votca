[![Codacy Badge](https://api.codacy.com/project/badge/Grade/97dee3a54c2d49829932aac8899f2843)](https://app.codacy.com/app/JoshuaSBrown/csg?utm_source=github.com&utm_medium=referral&utm_content=votca/csg&utm_campaign=Badge_Grade_Dashboard)
[![codecov](https://codecov.io/gh/votca/csg/branch/master/graph/badge.svg)](https://codecov.io/gh/votca/csg)
[![Build Status](https://travis-ci.org/votca/csg.svg?branch=master)](https://travis-ci.org/votca/csg)
[![pipeline status](https://gitlab.com/votca/csg/badges/master/pipeline.svg)](https://gitlab.com/votca/csg/commits/master)

Further information on VOTCA can be found at <http://www.votca.org>

The development of VOTCA is mainly funded by academic research grants. If you
use this package, please cite the following VOTCA papers:

* _Relative entropy and optimization-driven coarse-graining methods in VOTCA_,  
  S.Y. Mashayak, M. Jochum, K. Koschke, N.R. Aluru, V. Ruehle, and C.
  Junghans,  
  [PLoS one 10, e131754 (2015)](http://dx.doi.org/10.1371/journal.pone.0131754).

* _Hybrid approaches to coarse-graining using the VOTCA package: liquid
  hexane_,  
  V. Ruehle and C. Junghans,  
  [Macromol. Theory Simul. 20, 472 (2011)](http://dx.doi.org/10.1002/mats.201100011).

* _Versatile Object-oriented Toolkit for Coarse-graining Applications_,  
  V.Ruehle, C. Junghans, A. Lukyanov, K. Kremer, and D. Andrienko,  
  [J. Chem. Theo. Comp. 5 (12), 3211 (2009)](http://dx.doi.org/10.1021/ct900369w).

In case of questions, please post them in the google discussion group for votca
at: <http://groups.google.com/group/votca>

You can contact the VOTCA Development Team at devs@votca.org.
