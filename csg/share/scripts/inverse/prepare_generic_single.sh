#! /bin/bash
#
# Copyright 2009-2011 The VOTCA Development Team (http://www.votca.org)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

show_help () {
cat <<EOF
${0##*/}, version %version%
This script implements the prepares the potential in step 0, using pot.in or by resampling the target distribution

Usage: ${0##*/}

Allowed options:
    --program sim_prog   Simulation program used. Needed to read out interaction property inverse.$sim_prog.avg_points (default is gromacs).
EOF
}

sim_prog="gromacs"

### begin parsing options
shopt -s extglob
while [[ ${1#-} != $1 ]]; do
 if [[ ${1#--} = $1 && -n ${1:2} ]]; then
    #short opt with arguments here: o
    if [[ ${1#-[o]} != ${1} ]]; then
       set -- "${1:0:2}" "${1:2}" "${@:2}"
    else
       set -- "${1:0:2}" "-${1:2}" "${@:2}"
    fi
 fi
 case $1 in
   --program)
    sim_prog="$2"
    shift 2;;
   -h | --help)
    show_help
    exit 0;;
  *)
   die "Unknown option '$1'";;
 esac
done
### end parsing options

name=$(csg_get_interaction_property name)
min=$(csg_get_interaction_property min )
max=$(csg_get_interaction_property max )
step=$(csg_get_interaction_property step )
comment="$(get_table_comment)"
main_dir=$(get_main_dir)
bondtype="$(csg_get_interaction_property bondtype)"
output="${name}.pot.new"

if [[ -f ${main_dir}/${name}.pot.in ]]; then
  msg "Using given table ${name}.pot.in for ${name}"
  smooth="$(critical mktemp ${name}.pot.in.smooth.XXX)"
  echo "Converting ${main_dir}/${name}.pot.in to ${output}"
  critical csg_resample --in "${main_dir}/${name}.pot.in" --out ${smooth} --grid ${min}:${step}:${max} --comment "$comment"
  extrapolate="$(critical mktemp ${name}.pot.in.extrapolate.XXX)"
  avg_points="$(csg_get_interaction_property --allow-empty inverse.$sim_prog.avg_points)"
  #if option does not exist, default value of avg_points is 3
  avg_points="${avg_points:-3}"  
  do_external potential extrapolate --type "$bondtype" --avg-point "${avg_points}" "${smooth}" "${extrapolate}"
  shifted="$(critical mktemp ${name}.pot.in.shifted.XXX)"
  do_external potential shift --type "${bondtype}" ${extrapolate} ${shifted}
  do_external table change_flag "${shifted}" "${output}"
else
  target=$(csg_get_interaction_property inverse.target)
  msg "Using initial guess from dist ${target} for ${name}"
  #resample target dist
  do_external resample target "$(csg_get_interaction_property inverse.target)" "${name}.dist.tgt" 
  # initial guess from rdf
  raw="$(critical mktemp ${name}.pot.new.raw.XXX)"
  kbt="$(csg_get_property cg.inverse.kBT)"
  dist_min="$(csg_get_property cg.inverse.dist_min)"
  do_external dist invert --type "${bondtype}" --kbT "${kbt}" --min "${dist_min}" ${name}.dist.tgt ${raw}
  smooth="$(critical mktemp ${name}.pot.new.smooth.XXX)"
  critical csg_resample --in ${raw} --out ${smooth} --grid ${min}:${step}:${max} --comment "${comment}"
  extrapolate="$(critical mktemp ${name}.pot.new.extrapolate.XXX)"
  avg_points="$(csg_get_interaction_property --allow-empty inverse.$sim_prog.avg_points)"
  #if option does not exist, default value of avg_points is 3
  avg_points="${avg_points:-3}"  
  do_external potential extrapolate --type "$bondtype" --avg-point "${avg_points}" "${smooth}" "${extrapolate}"
  shifted="$(critical mktemp ${name}.pot.new.shifted.XXX)"
  do_external potential shift --type "${bondtype}" ${extrapolate} ${shifted}
  do_external table change_flag "${shifted}" "${output}"
fi

