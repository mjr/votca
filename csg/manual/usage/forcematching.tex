\chapter{Force matching}
\label{chapter:fm}

\begin{figure}[!ht]
   \centering
   \includegraphics[width=0.5\textwidth]{usage/fig/flow_fmatch.eps}
   \caption{Flowchart to perform force matching.}
   \label{fig:flowchart_twobody}
\end{figure}

The force matching algorithm with cubic spline basis is implemented in the \prog{csg_fmatch} utility. A list of available options can be found in the reference section of \prog{csg_fmatch} (command \texttt{--h}).

\section{Program input}
\label{sec:fm_input}

\prog{csg_fmatch} needs an atomistic reference run to perform coarse-graining. Therefore, the trajectory file {\em must contain forces } (note that there is a suitable option in the \gromacs \texttt{.mdp} file), otherwise \prog{csg_fmatch} will not be able to run.

In addition, a mapping scheme has to be created, which defines the coarse-grained model (see \sect{sec:inputfiles}). At last, a control file has to be created, which contains all the information for coarse-graining the interactions and parameters for the force-matching run. This file is specified by the tag \progopt{--options} in the \xml format. An example might look like the following
\begin{lstlisting}
   <cg>
     <!-- fmatch section -->
     <fmatch>
       <!-- Number of frames for block averaging -->
       <frames_per_block>6</frames_per_block>
       <!-- Constrained least squares?-->
       <constrainedLS>true</constrainedLS>
     </fmatch>
     <!-- example for a non-bonded interaction entry -->
     <non-bonded>
       <!-- name of the interaction -->
       <name>CG-CG</name>
       <!-- CG bead types (according to mapping file) -->
       <type1>A</type1>
       <type2>A</type2>
       <!-- fmatch specific stuff -->
       <fmatch>
         <!-- min for pair interaction (in nm) -->
         <min>0.27</min>
         <!-- max for pair interaction (in nm) -->
         <max>1.2</max>
         <!-- step size for internal spline representation -->
         <step>0.02</step>
         <!-- output step size for pair interaction -->
         <out_step>0.005</out_step>
       </fmatch>
     </non-bonded>
   </cg>
\end{lstlisting}
Similarly to the case of spline fitting (see sec. \ref{sec:ref_programs} on \prog{csg_resample}), the parameters \texttt{min} and \texttt{max} have to be chosen in such a way as to avoid empty bins within the grid. Determining \texttt{min} and \texttt{max} by using \prog{csg_stat} is recommended (see sec. \ref{sec:setting_files}). A full description of all available options can be found in sec. \ref{sec:ref_options}.

\section{Program output}
\label{sec:fm_output}

\prog{csg_fmatch} produces a separate \texttt{.force} file for each interaction, specified in the CG-options file (\progopt{---options}).
These files have 4 columns containing distance, corresponding force, a table flag and the force error, which is estimated via a block-averaging procedure.
If you are working with an angle, then the first column will contain the corresponding angle in radians.

To get table-files for \gromacs, integrate the forces in order to get potentials and do extrapolation and potentially smoothing afterwards.

Output files are not only produced at the end of the program execution, but also after every successful processing of each block. The user is free to have a look at the output files and decide to stop \prog{csg_fmatch}, provided the force error is small enough.

\section{Integration and extrapolation of .force files }
\label{sec:fm_integration}

To convert forces (\texttt{.force}) to potentials (\texttt{.pot}), tables have to be integrated. To use the built-in integration command from the scripting framework, execute
\begin{verbatim}
 $csg_call table integrate CG-CG.force minus_CG-CG.pot
 $csg_call table linearop minus_CG-CG.d CG-CG.d -1 0
\end{verbatim}
This command calls the \prog{table_integrate.pl} script, which integrates the force and writes the potential to the \texttt{.pot} file.

In general, each potential contains regions which are not sampled. In this case or in the case of further post-processing, the potential can be refined by employing resampling or extrapolating methods. See sec. \ref{sec:post_processing} for further details.

\section{Three-body Stillinger-Weber interactions}
\label{sec:fm_3body}

As described in the theory section, \prog{csg_fmatch} is also able to parametrize the angular part of three-body interactions of the Stillinger-Weber type (see subsec. \ref{sec:fm_sw}). The general procedure is the same, as shown in the two-body case (See flowchart in Fig. \ref{fig:flowchart_twobody}). It has to be specified in the control file that one wants to parametrize a three-body interaction. An example might look like this:
\begin{lstlisting}
   <cg>
     <!-- fmatch section -->
     <fmatch>
       <!-- Number of frames for block averaging -->
       <frames_per_block>10</frames_per_block>
       <!-- Constrained least squares?-->
       <constrainedLS>true</constrainedLS>
     </fmatch>
     <!-- example for a non-bonded interaction entry with three-body SW interactions -->
     <non-bonded>
       <!-- name of the interaction -->
       <name>CG-CG-CG</name>
       <!-- flag for three-body interactions -->
       <threebody>true</threebody>
       <!-- CG bead types (according to mapping file) -->
       <type1>A</type1>
       <type2>A</type2>
       <type3>A</type3>
       <!-- fmatch section of interaction -->
       <fmatch>
         <!-- short-range cutoff (in nm) -->
         <a>0.37</a>
         <sigma>1.0</sigma>
         <!-- switching range (steepness of exponential switching function) (in nm) -->
         <gamma>0.08</gamma>
         <!-- min for angular interaction (in rad) -->
         <min>0.7194247283</min>
         <!-- max for angular interaction (in rad) -->
         <max>3.1415927</max>
         <!-- step size for internal spline representation (in rad) -->
         <step>0.1</step>
         <!-- output step size for angular interaction (in rad) -->
         <out_step>0.0031415927</out_step>
       </fmatch>
     </non-bonded>
   </cg>
\end{lstlisting}
As in the case of pair interactions, the parameters \texttt{min} and \texttt{max} have to be chosen in such a way as to avoid empty bins within the grid. Determining \texttt{min} and \texttt{max} by using \prog{csg_stat} is recommended (see sec. \ref{sec:setting_files}).

For each three-body interaction as specified in the CG-options file (\progopt{---options}), \prog{csg_fmatch} produces a separate \texttt{.force} file, as well as a \texttt{.pot} file. Due to the functional form of the Stillinger-Weber potential, \prog{csg_fmatch} outputs the three-body force in angular direction, as well as the angular potential. Therefore, in contrast to the two-body force (see section \ref{sec:fm_integration}), the angular three-body force does not have to be numerically integrated. The \texttt{.force}, as well as the \texttt{.pot} file have 4 columns containing the angle in radians, the force or potential, the error (which is estimated via a block-averaging procedure) and a table flag.

Coarse-grained simulations with three-body Stillinger-Weber interactions can be done with \lammps with the user \texttt{pair\_style sw/table}. For this, the \texttt{.pot} file has to be converted into a table format according to the \lammps \texttt{angle\_style table}. This can be done with:
\begin{verbatim}
 $csg_call --options table.xml --ia-name XXX --ia-type angle 
  convert_potential lammps --clean --no-shift XXX.pot table_XXX.txt
\end{verbatim}
in line with the conversion of angular tables for bonded interactions. Therefore, the CG-options file (\progopt{---options}) now has to contain a \progopt{<bonded>} section with the appropriate interaction name:

\begin{lstlisting}
   <cg>
     <bonded>
       <!-- name of the interaction -->
       <name>CG-CG-CG</name>
       <!-- CG bead types (according to mapping file) -->
       <type1>A</type1>
       <type2>A</type2>
       <type3>A</type3>
       <min>0.7194247283</min>
       <max>3.1415927</max>
       <step>0.0031415927</step>
       <!-- settings for converting table to lammps angular format -->
       <inverse>
         <lammps>
           <table_begin>0</table_begin>
           <table_end>180</table_end>
           <table_bins>0.18</table_bins>
           <y_scale>0.239006</y_scale>
           <avg_points>1</avg_points>
         </lammps>
       </inverse>
     </bonded>
   </cg>
\end{lstlisting}

For a further description of posprocessing, we refer again to sec. \ref{sec:post_processing}. For a more detailed example, we refer to the tutorial in \prog{csg-tutorials/spce/3body_sw/}.
