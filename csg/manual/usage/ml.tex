\chapter{Kernel-Based Machine Learning}
\label{chapter:ml}

\begin{figure}[!ht]
   \centering
   \includegraphics[width=0.5\textwidth]{usage/fig/flow_ml.eps}
   \caption{Flowchart of different steps necessary to perform a ML calculation.}
   \label{fig:flowchart_ml}
\end{figure}

The kernel-based machine learning algorithm for non-bonded forces is implemented in the \prog{csg_ml} utility. A list of available options can be found in the reference section of \prog{csg_ml} (command \texttt{--h}). The general procedure for parametrizing an ML model is shown in Fig.~\ref{fig:flowchart_ml}. (So far, \prog{csg_ml} can parametrize a single non-bonded pair or three-body interaction). For training, as well as testing the ML model, \prog{csg_ml} needs an (atomistic) reference trajectory which {\em must contain forces }. (There is either a suitable option in the \gromacs \texttt{.mdp} file or the \lammps dump file must contain forces). In addition, a mapping scheme has to be created, which defines the coarse-grained model (see \sect{sec:inputfiles}) or the option \progopt{--no-map} has to be used when the reference trajectory already has the target resolution. It is useful to split the trajectory into different fragments to create independent (force) data sets for training and testing. At last, a control files has to be created, which contains all the information for the interactions and parameters for the kernel-based machine learning parametrization. This file is specified by the tag \progopt{--options} in the \xml format. It is useful to create different control files for training, testing and evaluation of the (three-body) force table.

\section{Training the ML model}
\label{sec:ml_training}

First, a ML model has to be trained. This can be done for a two-body or a three-body interaction, in both cases with or without employing the covariant meshing (see subsection \ref{sec:ml_binning}). An example for a \xml control file for a pair interaction without covariant meshing might look like the following:
\begin{lstlisting}
   <cg>
     <!--ml section -->
     <ml>
       <!--Number of frames from trajectory file -->
       <frames>400</frames>
       <!--Number of atoms taken into account per frame -->
       <nbeads_per_frame>1</nbeads_per_frame>
       <!--csg_ml run in training mode -->
       <train>true</train>
       <!--ML regularization parameter lambda -->
       <lambda>0.0000000001</lambda>
     </ml>
     <!-- example for a non-bonded interaction entry -->
     <non-bonded>
       <!-- name of the interaction -->
       <name>1-1</name>
       <!-- CG bead types (according to mapping file) -->
       <type1>1</type1>
       <type2>1</type2>
       <!-- ml specific stuff -->
       <ml>
         <!-- min for pair interaction (in Ang) -->
         <min>2.5</min>
         <!-- max for pair interaction (in Ang) -->
         <max>6.0</max>
         <!-- hyperparameter for pair kernel (in Ang) -->
         <sigma>0.5</sigma>
         <!-- output step size for pair interaction -->
         <out_step>0.05</out_step>
       </ml>
     </non-bonded>
   </cg>
\end{lstlisting}
Here, the ML model is trained based on in total 400 configurations, one randomly selected from each of the 400 frames. The parameters \texttt{min} and \texttt{max} have to be chosen according to the pair distribution function (see also chapter \ref{chapter:fm}). One has to try out a range of hyper parameters, to see which one gives the best results. In addition, one has to vary the number of training configurations and train the ML model according to different independent data sets in order to obtain a meaningful learning curve. In doing so, one has to have in mind that the kernel size and thus the memory usage scales quadratically with the number of included pairs (and thus configurations).

An example for a \xml control file for a three-body interaction with using the covariant meshing might look like the following:
\begin{lstlisting}
   <cg>
     <!--ml section -->
     <ml>
       <!--Number of frames from trajectory file -->
       <frames>1000</frames>
       <!--Number of atoms taken into account per frame -->
       <nbeads_per_frame>10</nbeads_per_frame>
       <!--csg_ml run in training mode -->
       <train>true</train>
       <!--ML regularization parameter lambda -->
       <lambda>0.0000000001</lambda>
     </ml>
     <!-- example for a non-bonded interaction entry -->
     <non-bonded>
       <!-- name of the interaction -->
       <name>1-1-1</name>
       <type1>1</type1>
       <type2>1</type2>
       <type3>1</type3>
       <!-- flag for three-body interactions -->
       <threebody>true</threebody>
       <ml>
         <!-- min for interaction (in Ang) -->
         <min>2.5</min>
         <!-- max for interaction (in Ang) -->
         <max>3.7</max>
         <!-- three components of the hyperparameter (in Ang) -->
         <sigma1>0.7</sigma1>
         <sigma2>0.7</sigma2>
         <sigma3>1.4</sigma3>
         <!-- output step size for sample force preditions -->
         <out_step>0.02</out_step>
         <!-- switching function with transition region of 0.4 Ang -->
         <d>0.4</d>
         <!-- flag for covariant meshing -->
         <binning>true</binning>
         <!-- number of grid points in radial directions -->
         <N_bins>24</N_bins>
         <!-- number of grid points in angular direction-->
         <N_theta>33</N_theta>
         <!-- minimum angle for 3-dimensional grid-->
         <min_theta>37</min_theta>         
         <!-- flag for gaussian smearing when using covariant meshing -->
         <gaussian_smearing>true</gaussian_smearing>
         <!-- smear scale (in terms of grid spacing) -->
         <smear_scale>1.8</smear_scale>
       </ml>
     </non-bonded>
   </cg>
\end{lstlisting}
Here, the ML model is trained based on in total 10000 configurations, 10 randomly selected from each of the 1000 frames. Again, \texttt{max} sets the interaction cutoff. Now, the hyperparameter has three components, one for each component of the 3-body representation (see Eqn.~\eqref{eq:triplet}). A switching function is applied with a width of the transition region of 0.4 Ang. Covariant meshing is used on a three-dimensional grid. The number of grid points in radial direction, \texttt{N\_bins}, is $24$ between length \texttt{min} to \texttt{max}. The number of grid points in angular direction, \texttt{N\_theta}, is $33$ between $37^{\circ}$ (\texttt{min\_theta}) and $180^{\circ}$. Due to symmetry reasons this leads to in total $\left(24 \cdot \left(24+1\right) \cdot 33/2\right)=9900$ grid points. Gaussian smearing is applied with a smearing width of 1.8 times the grid spacing. More details on the covariant meshing procedure are given in subsection~\ref{sec:ml_binning}.

The training of an ML model can then be done with a command like this:
\begin{verbatim}
 $OMP_NUM_THREADS=${n_threads} csg_ml --no-map --options settings.xml 
 --top train.dump --trj train.dump --nframes 1000 > train.out
\end{verbatim}
As \prog{csg_ml} uses OpenMP parallelization, $\$\{\textrm{n\_threads}\}$ refers to the number of OpenMP threads used. Training is based on a \lammps dump file with at least 1000 frames. After reading in \texttt{frames} frames, \prog{csg_ml} accumulates the data and evaluates the ML model. Therefore, it is useful to terminate the calculation there with the \progopt{---nframes} option.

In training mode (\progopt{<train>}true\progopt{</train>}), \prog{csg_ml} creates a folder \texttt{MLObjects} and serializes all parameters of the ML model in a binary file: \texttt{$\$$n.ml} where $\$$n is a counter over different ML interactions. For pair interactions, there is an additional output of the pair force ($\$\{\textrm{name}\}$.force). For three-body interactions, there is also an additional output of force scans of a series of sample triplets. These are written into files $\$\{\textrm{name}\}\textrm{.force}\$\{\textrm{n}\}\_\$\{\textrm{n\_bead}\}\_\textrm{x}$, $\$\{\textrm{name}\}\textrm{.force}\$\{\textrm{n}\}\_\$\{\textrm{n\_bead}\}\_\textrm{y}$, and $\$\{\textrm{name}\}\textrm{.force}\$\{\textrm{n}\}\_\$\{\textrm{n\_bead}\}\_\textrm{z}$, as well as 
\\$\$\{\textrm{name}\}\textrm{.forcetheta}\$\{\textrm{n}\}\_\$\{\textrm{n\_bead}\}\_\textrm{x}$, $\$\{\textrm{name}\}\textrm{.forcetheta}\$\{\textrm{n}\}\_\$\{\textrm{n\_bead}\}\_\textrm{y}$, and 
\\$\$\{\textrm{name}\}\textrm{.forcetheta}\$\{\textrm{n}\}\_\$\{\textrm{n\_bead}\}\_\textrm{z}$. The orientation of these sample triplets is as follows: the interparticle vector ${\bm r}_{m_{ab}}$ is oriented along the $x$-axis with $r_{m_{ab,x}}>0$. The vector ${\bm r}_{m_{ac}}$ is oriented in the $xy$-plane such that $r_{m_{ac,y}}>0$. For the force scans named $\$\{\textrm{name}\}\textrm{.force}\$\textrm{*}$, the angle between ${\bm r}_{m_{ab}}$ and ${\bm r}_{m_{ac}}$, $\theta_{{bc}}$, is fixed and the distances $r_{m_{ab}}$ and $r_{m_{ac}}$ are varied simultaneously from \texttt{min} to \texttt{max}. Here, $\$\{\textrm{n}\}=1$ refers to $\theta_{{bc}}=70^{\circ}$, $\$\{\textrm{n}\}=2$ refers to $\theta_{{bc}}=90^{\circ}$, $\$\{\textrm{n}\}=3$ refers to $\theta_{{bc}}=110^{\circ}$, $\$\{\textrm{n}\}=4$ refers to $\theta_{{bc}}=130^{\circ}$, and $\$\{\textrm{n}\}=5$ refers to $\theta_{{bc}}=150^{\circ}$. The variable $\$\{\textrm{n\_bead}\}=1$ refers to the force on bead $\textrm{a}$, $\$\{\textrm{n\_bead}\}=2$ to $\textrm{b}$, and $\$\{\textrm{n\_bead}\}=3$ to $\textrm{c}$ according to the triplet definition of Fig.~\ref{fig:decomposition}. The suffixes $\_\textrm{x}$, $\_\textrm{y}$, and $\_\textrm{z}$ refer to the force in $\textrm{x}$, $\textrm{y}$, and $\textrm{z}$ direction according to the orientation of these sample triplets. For the force scans named $\$\{\textrm{nametheta}\}\textrm{.force}\$\textrm{*}$, the angle between ${\bm r}_{m_{ab}}$ and ${\bm r}_{m_{ac}}$, $\theta_{{bc}}$, is varied between $0^{\circ}$ and $180^{\circ}$ and the distances $r_{m_{ab}}$ and $r_{m_{ac}}$ are both fixed at the same distance, namely to \texttt{min} for $\$\{\textrm{n}\}=1$, to $\left(\texttt{min}+\frac{1}{3}\left(\texttt{max}-\texttt{min}\right)\right)$ for $\$\{\textrm{n}\}=2$, to $\left(\texttt{min}+\frac{2}{3}\left(\texttt{max}-\texttt{min}\right)\right)$ for $\$\{\textrm{n}\}=3$, and to $\texttt{max}$ for $\$\{\textrm{n}\}=4$. The meaning of the variable $\$\{\textrm{n\_bead}\}$ is the same as before.

\section{Testing the ML model}
\label{sec:ml_testing}

After training, one should test the quality of a ML model on a different data set. An \xml control file for testing the ML model for a three-body interaction could look like:
\begin{lstlisting}
   <cg>
     <!--ml section -->
     <ml>
       <!--Number of frames from trajectory file -->
       <frames>20</frames>
       <!--Number of atoms taken into account per frame -->
       <nbeads_per_frame>100</nbeads_per_frame>
       <!--csg_ml run in testing mode -->
       <train>false</train>
       <!--ML regularization parameter lambda -->
       <lambda>0.0000000001</lambda>
     </ml>
     <!-- example for a non-bonded interaction entry -->
     <non-bonded>
       <!-- name of the interaction -->
       <name>1-1-1</name>
       <type1>1</type1>
       <type2>1</type2>
       <type3>1</type3>
       <!-- flag for three-body interactions -->
       <threebody>true</threebody>
       <ml>
         <!-- min for interaction (in Ang) -->
         <min>2.5</min>
         <!-- max for interaction (in Ang) -->
         <max>3.7</max>
         <!-- three components of the hyperparameter (in Ang) -->
         <sigma1>0.7</sigma1>
         <sigma2>0.7</sigma2>
         <sigma3>1.4</sigma3>
         <!-- switching function with transition region of 0.4 Ang -->
         <d>0.4</d>
       </ml>
     </non-bonded>
   </cg>
\end{lstlisting}
For a pair interaction, it has to be adjusted accordingly. The ML model is tested based on in total 2000 configurations, 100 randomly selected from each of the 20 frames. It is important, to set \texttt{min}, \texttt{max}, and \texttt{d} (setting for the switching function) to the values used in training the ML model. Otherwise, wrong force predictions are made. The \xml control file for testing does not contain any options for the covariant meshing, as the force prediction of a trained model is independent on whether the training has been done with or without the covariant meshing.

The testing of a ML model on an independent data set can then be done with a command like this:
\begin{verbatim}
 $OMP_NUM_THREADS=${n_threads} csg_ml --no-map --options settings.xml 
 --top test.dump --trj test.dump --nframes 20 > test.out
\end{verbatim}
Again, $\$\{\textrm{n\_threads}\}$ refers to the number of OpenMP threads. The \lammps dump file has to contain at least 20 frames. It is important that testing is done in the same folder as training, as the binary file \texttt{MLObjects/$\$$n.ml} has to be read in. \prog{csg_ml} then outputs the predicted forces for each configuration of the test data set. After reading in \texttt{frames} frames, an average test error is calculated and written out, averaging the force errors over all three Cartesian components. Therefore, it is again useful to terminate the calculation at this point with the \progopt{---nframes} option.

\section{Calculating a three-body force table}
\label{sec:ml_table}

For pair interactions, a tabulated pair force file ($\$\{\textrm{name}\}$.force) is automatically written out in the \prog{csg_ml} training mode. This is equivalent to the pair force output of regular force-matching as described in section~\ref{sec:fm_output}. As tabulating the three-body forces is much more involved and memory-consuming, it is not automatically done. To do so, \prog{csg_ml} has to be run again in the folder containing the binary file \texttt{MLObjects/$\$$n.ml} from training. For reasons of the app structure, \prog{csg_ml} has to be run in test mode in order to read in the binary file containing the ML parametrizaion and to calculate the three-body force table based on this model.

An \xml control file might look like this:
\begin{lstlisting}
   <cg>
     <!--ml section -->
     <ml>
       <!--Number of frames from trajectory file -->
       <frames>1</frames>
       <!--Number of atoms taken into account per frame -->
       <nbeads_per_frame>1</nbeads_per_frame>
       <!--csg_ml run in testing mode -->
       <train>false</train>
       <!--ML regularization parameter lambda -->
       <lambda>0.0000000001</lambda>
     </ml>
     <!-- example for a non-bonded interaction entry -->
     <non-bonded>
       <!-- name of the interaction -->
       <name>1-1-1</name>
       <type1>1</type1>
       <type2>1</type2>
       <type3>1</type3>
       <!-- flag for three-body interactions -->
       <threebody>true</threebody>
       <ml>
         <!-- min for interaction (in Ang) -->
         <min>2.5</min>
         <!-- max for interaction (in Ang) -->
         <max>3.7</max>
         <!-- three components of the hyperparameter (in Ang) -->
         <sigma1>0.7</sigma1>
         <sigma2>0.7</sigma2>
         <sigma3>1.4</sigma3>
         <!-- switching function with transition region of 0.4 Ang -->
         <d>0.4</d>
         <!-- flag for calculating the 3-body force table -->
         <output_table>true</output_table>
         <!-- number of table bins in radial directions -->
         <N_table>30</N_table>
       </ml>
     </non-bonded>
   </cg>
\end{lstlisting}
For calculating the three-body force table, the number of bins in angular direction is fixed to $2\cdot$\texttt{N\_table}, the minimum angle is set to $0^{\circ}$, and the maximum angle to $180^{\circ}$ and can not be choosen seperately. Due to symmetry reasons, the total number of table bins in this example is $\left(30 \cdot \left(30+1\right) \cdot 60/2\right)=27900$ grid points. More details on the construction of the three-dimensional grid is given in subsection~\ref{sec:ml_table}.

The command for running the calculation then is:
\begin{verbatim}
 $OMP_NUM_THREADS=${n_threads} csg_ml --no-map --options settings.xml 
 --top test.dump --trj test.dump --nframes 1 > write_table.out
\end{verbatim}
As \prog{csg_ml} has to be run in test mode, at least one configuration of one frame has to be read in. The tabulated three-body force table ($\$\{\textrm{name}\}$.table) is then written out. 

The structure of $\$\{\textrm{name}\}$.table is as follows: The header contains 3 lines. It has the following structure:
\begin{verbatim}
 ${keyword}
 N ${N_table} rmin (${min}+0.5${dbin}) rmax (${max}-0.5${dbin})
 
\end{verbatim}
with $\$\{\textrm{dbin}\}=\$\{\textrm{max}\}-\$\{\textrm{min}\}/\$\{\textrm{N\_table}\}$. As only one interaction can be parametrized at a time, the keyword is set to the default value \texttt{ENTRY1}. With the parameters of this example, the header looks like:
\begin{verbatim}
 ENTRY1
 N 30 rmin 2.52 rmax 3.68
 
\end{verbatim}

After this follows the actual tabulation where the number of lines corresponds to the number of table bins. Each line has the following structure:

\bigskip
$m$ $r^{\text{out}}_{m_{ab}}$ $r^{\text{out}}_{m_{ac}}$ $\theta^{\text{out}}_{m}$ $f_{m_{a1}}$ $f_{m_{a2}}$ $f_{m_{b1}}$ $f_{m_{b2}}$ $f_{m_{c1}}$ $f_{m_{c2}}$ $e$
\bigskip

The variable $m$ is the index over all output triplet configurations. Each line contains the distances $r^{\text{out}}_{m_{ab}}$, $r^{\text{out}}_{m_{ab}}$, the angle $\theta^{\text{out}}_{m}$ and the $6$ force coefficients of triplet configuration $m$ as described in subsection~\ref{sec:ml_table}. The variable $e$ is a place holder for the energy of the triplet configuration. At the moment, only the force constants are written out. With the parameters of this example, the first line of the actual tabulation could look like this:

\bigskip
1 2.52 2.52 1.5 -6633.87 4068.46 6633.87 -980018 -4068.46 980018 0
\bigskip

The force table can be used for MD simulations as input table for the \lammps user \texttt{pair\_style 3b/table}. For more details on this pair\_style, we refer to the tutorial in \prog{csg-tutorials/ml/3body/md}.

